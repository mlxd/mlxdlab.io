<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Lost in Computation on Lost in Computation</title>
    <link>https://mlxd.gitlab.io/</link>
    <description>Recent content in Lost in Computation on Lost in Computation</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>&amp;copy; 2017 Lee James O&#39;Riordan (mlxd)</copyright>
    <lastBuildDate>Wed, 20 Apr 2016 00:00:00 +0000</lastBuildDate>
    <atom:link href="/" rel="self" type="application/rss+xml" />
    
    <item>
      <title>FFTs over time and space... and devices</title>
      <link>https://mlxd.gitlab.io/post/2017-07-11-ffts/</link>
      <pubDate>Mon, 01 Jan 2018 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/post/2017-07-11-ffts/</guid>
      <description>

&lt;hr /&gt;

&lt;h1 id=&#34;q-how-do-available-fft-routines-compare-over-different-libraries-and-accelerator-hardwares&#34;&gt;Q. How do available FFT routines compare over different libraries and accelerator hardwares?&lt;/h1&gt;

&lt;hr /&gt;

&lt;p&gt;This is something I have wondered for sometime, though it can be difficult for an apples-to-apples comparison. I will consider the above question in relation to the following plan:&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;&lt;em&gt;Comparing FFT implementations in a pseudospectral solver for linear and nonlinear Schrodinger equation dynamics&lt;/em&gt;&lt;/strong&gt;&lt;/p&gt;

&lt;p&gt;To decide on how to proceed, it is instructive to list all (or most) widely known and used implementations.&lt;/p&gt;

&lt;h2 id=&#34;standard-implementations&#34;&gt;Standard implementations&lt;/h2&gt;

&lt;h3 id=&#34;cpu&#34;&gt;[CPU]&lt;/h3&gt;

&lt;ul&gt;
&lt;li&gt;FFTW: &lt;a href=&#34;http://fftw.org/&#34;&gt;http://fftw.org/&lt;/a&gt; and &lt;a href=&#34;https://github.com/FFTW/fftw3&#34;&gt;https://github.com/FFTW/fftw3&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;MKL-FFT: &lt;a href=&#34;https://software.intel.com/en-us/articles/the-intel-math-kernel-library-and-its-fast-fourier-transform-routines&#34;&gt;https://software.intel.com/en-us/articles/the-intel-math-kernel-library-and-its-fast-fourier-transform-routines&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;clFFT: &lt;a href=&#34;https://github.com/clMathLibraries/clFFT&#34;&gt;https://github.com/clMathLibraries/clFFT&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;Eigen FFT (using kissfft backend): &lt;a href=&#34;http://eigen.tuxfamily.org/index.php?title=EigenFFT&#34;&gt;http://eigen.tuxfamily.org/index.php?title=EigenFFT&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;sFFT: &lt;a href=&#34;http://groups.csail.mit.edu/netmit/sFFT/code.html&#34;&gt;http://groups.csail.mit.edu/netmit/sFFT/code.html&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;KFR FFT: &lt;a href=&#34;https://github.com/kfrlib/fft&#34;&gt;https://github.com/kfrlib/fft&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;h3 id=&#34;gpu&#34;&gt;[GPU]&lt;/h3&gt;

&lt;ul&gt;
&lt;li&gt;cuFFT: &lt;a href=&#34;http://docs.nvidia.com/cuda/cufft/index.html&#34;&gt;http://docs.nvidia.com/cuda/cufft/index.html&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;clFFT: &lt;a href=&#34;https://github.com/clMathLibraries/clFFT&#34;&gt;https://github.com/clMathLibraries/clFFT&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;fbfft: &lt;a href=&#34;https://github.com/facebook/fbcuda/tree/master/fbfft&#34;&gt;https://github.com/facebook/fbcuda/tree/master/fbfft&lt;/a&gt; and &lt;a href=&#34;https://arxiv.org/abs/1412.7580&#34;&gt;https://arxiv.org/abs/1412.7580&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;h3 id=&#34;cutting-edge&#34;&gt;[Cutting edge]&lt;/h3&gt;

&lt;ul&gt;
&lt;li&gt;FPGA FFT (Xilinx and Altera/Intel): &lt;a href=&#34;https://www.xilinx.com/products/intellectual-property/fft.html&#34;&gt;https://www.xilinx.com/products/intellectual-property/fft.html&lt;/a&gt; and &lt;a href=&#34;https://www.altera.com/products/intellectual-property/ip/dsp/m-ham-fft.html&#34;&gt;https://www.altera.com/products/intellectual-property/ip/dsp/m-ham-fft.html&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;IBM TrueNorth FFT: No link/source found (yet)&lt;/li&gt;
&lt;li&gt;Rigetti pyQuil quantum FFT: &lt;a href=&#34;http://pyquil.readthedocs.io/en/latest/getting_started.html&#34;&gt;http://pyquil.readthedocs.io/en/latest/getting_started.html&lt;/a&gt; (currently emulated AFAIK, but will eventually/hopefully run directly on quantum hardware)&lt;/li&gt;
&lt;/ul&gt;

&lt;h2 id=&#34;test-cases&#34;&gt;Test cases&lt;/h2&gt;

&lt;p&gt;To ensure sufficient time is spent in the FFT routines I will opt for a 2D transform of an NxN grid, requiring N transforms, a transpose, and another N. The value of N can be scaled to determine sweet spots for all implementations. Granted, memory size will be the determining factor of how large we can go. $N=2^j$ where $j \in \mathbb{Z}, 7 \leq j \leq 11$ is a reasonable range of values to examine that should fit readily on most hardware.&lt;/p&gt;

&lt;p&gt;For this, the following test precisions will be instructive:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;p&gt;$\mathbb{C}2\mathbb{C}$ - 32-bit float (32-bits for each real and imaginary component)&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;$\mathbb{C}2\mathbb{C}$ - 64bit float&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;$\mathbb{R}2\mathbb{C}$ - 32bit float&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;$\mathbb{R}2\mathbb{C}$ - 64bit float&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;As a sample system to investigate, I will consider the case of a quantum harmonic oscillator (QHO). To investigate dynamics and their resulting accuracy, a known and analytically calculable result is useful. For this, I will opt for a a superposition state of the groundstate along the $xy$ plane, and the first excited state along $x$ with the groundstate along $y$. To put things more formally:&lt;/p&gt;

&lt;p&gt;$$
\Psi(x,y) = \frac{ \Psi_{00}(x,y) + \Psi_{10}(x,y) }{\sqrt{2}}.
$$
To construct the above wavefunction we will assume that the states $\Psi_{00}(x,y)$ and $\Psi_{10}(x,y)$ are outer products of the solutions to the QHO, as given by:&lt;/p&gt;

&lt;p&gt;$$
\Psi_n(x) = \frac{1}{\sqrt{2^{n}n!}}\left(\frac{m\omega_x}{\pi\hbar}\right)^{\frac{1}{4}}\mathrm{e}^{-\frac{m\omega_x x^2}{2\hbar}}H_n(x),
$$
where $$
H_n(x)=(-1)^n\mathrm{e}^{x^2}\frac{d^n}{dx^n}\left(\mathrm{e}^{-x^2}\right),$$
are the &lt;a href=&#34;https://en.wikipedia.org/wiki/Hermite_polynomials&#34;&gt;physicists&amp;rsquo; Hermite polynomials&lt;/a&gt;. As we are looking for the ground and first excited states ($n=0,1$) we can simplify a lot of the above calculations: $H_0(x)=1$ and $H_0(x)=2x$. Next, we determine the states $\Psi_0(x)$ and $\Psi_1(x)$ as:&lt;/p&gt;

&lt;p&gt;$$ \begin{eqnarray}
\Psi_0(x) = \left(\frac{m\omega_x}{\pi\hbar}\right)^\frac{1}{4}\mathrm{e}^{-\frac{m\omega_x x^2}{2\hbar}},~
\Psi_1(x) = \frac{2x}{\sqrt{2}} \left(\frac{m\omega_x}{\pi\hbar}\right)^{\frac{1}{4}}\mathrm{e}^{-\frac{m\omega_x x^2}{2\hbar}}.
\end{eqnarray}
$$&lt;/p&gt;

&lt;p&gt;Along a single dimension, our system can potentially be in any of the allowed harmonic oscillator states. Therefore, the overall state is given by a tensor product of the state along $x$ and that along $y$. If we assume the dimensionality of the Hilbert space along the $i$-th orthogonal spacial index is finite and given by $d_i$, then the overall system dimensionality (ie the total number of states we can consider) is $d = \displaystyle\prod_{i} d_i$.&lt;/p&gt;

&lt;p&gt;Assuming an $n$-dimensional system, where each individual dimension is in the groundstate of the harmonic oscillator, we can define the tensor product state as
$$
\Psi_n = \Psi_0^{\otimes n} = \Psi_0 \otimes \Psi_0  \cdots \otimes \Psi_0.
$$&lt;/p&gt;

&lt;p&gt;While the above is fine for a general case, for the purposes of FFTs we have assumed a much simpler system, dealing with combinations of only 2 states. We can then define them as $\Psi_{00}(x,y) = \Psi_{0}(x)\otimes\Psi_{0}(y)$ and $\Psi_{10}(x,y) = \Psi_{1}(x)\otimes\Psi_{0}(y)$. Thus, our final superposition state when filling everything in is given by
$$
\Psi(x,y) = \left(\frac{m}{\pi\hbar}\right)^{\frac{1}{2}}\left(\omega_x\omega_y\right)^{\frac{1}{4}}\mathrm{e}^{-\frac{m}{2\hbar}\left(\omega_x x^2 + \omega_y y^2\right)}\left(x + \frac{1}{\sqrt{2}}\right).
$$&lt;/p&gt;

&lt;p&gt;For a more detailed explanation and approach to the above have a look at Christina Lee&amp;rsquo;s (&lt;a href=&#34;https://github.com/albi3ro&#34;&gt;albi3ro&lt;/a&gt;) blog post on &lt;a href=&#34;http://albi3ro.github.io/M4//prerequisites%20required/Time-Evolution.html&#34;&gt;time evolution&lt;/a&gt;, which will cover the method I use next.&lt;/p&gt;

&lt;p&gt;&lt;code&gt;To be continued&lt;/code&gt;&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>GPUE</title>
      <link>https://mlxd.gitlab.io/project/gpue/</link>
      <pubDate>Wed, 18 Jan 2017 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/project/gpue/</guid>
      <description>&lt;p&gt;This page will be updated with instructions on how to use &lt;a href=&#34;https://github.com/mlxd/gpue&#34;&gt;GPUE&lt;/a&gt;.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Ultracold quantum systems</title>
      <link>https://mlxd.gitlab.io/project/uqs/</link>
      <pubDate>Wed, 18 Jan 2017 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/project/uqs/</guid>
      <description>&lt;p&gt;This page will be updated with details of the research undertaken during my PhD.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Example Talk</title>
      <link>https://mlxd.gitlab.io/talk/example-talk/</link>
      <pubDate>Sun, 01 Jan 2017 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/talk/example-talk/</guid>
      <description>&lt;p&gt;Embed your slides or video here using &lt;a href=&#34;https://gcushen.github.io/hugo-academic-demo/post/writing-markdown-latex/&#34;&gt;shortcodes&lt;/a&gt;. Further details can easily be added using &lt;em&gt;Markdown&lt;/em&gt; and $\rm \LaTeX$ math code.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Topological defect dynamics of vortex lattices in Bose–Einstein condensates</title>
      <link>https://mlxd.gitlab.io/publication/defect2016/</link>
      <pubDate>Thu, 03 Nov 2016 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/publication/defect2016/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Questions to expand upon</title>
      <link>https://mlxd.gitlab.io/post/2017-07-11-questions/</link>
      <pubDate>Wed, 11 May 2016 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/post/2017-07-11-questions/</guid>
      <description>

&lt;h1 id=&#34;questions-to-consider&#34;&gt;Questions to consider&lt;/h1&gt;

&lt;ul&gt;
&lt;li&gt;FFTs over devices for quantum dynamics&lt;/li&gt;
&lt;/ul&gt;
</description>
    </item>
    
    <item>
      <title>Moiré superlattice structures in kicked Bose–Einstein condensates</title>
      <link>https://mlxd.gitlab.io/publication/moire2016/</link>
      <pubDate>Thu, 04 Feb 2016 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/publication/moire2016/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Initiali[sz]e</title>
      <link>https://mlxd.gitlab.io/post/2015-10-17-init/</link>
      <pubDate>Mon, 18 Jan 2016 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/post/2015-10-17-init/</guid>
      <description>

&lt;p&gt;(Original post 2015-10-17; updated 2017-01-18)&lt;/p&gt;

&lt;p&gt;As this is my first post I will keep things simple. Unless otherwise specified, I will be using the following as my default tools.&lt;/p&gt;

&lt;hr /&gt;

&lt;h3 id=&#34;editors-ide-cool-things&#34;&gt;Editors/IDE/Cool things&lt;/h3&gt;

&lt;hr /&gt;

&lt;ul&gt;
&lt;li&gt;ViM (pretty much everything code related)&lt;/li&gt;
&lt;li&gt;Atom (Markdown, text files, and the like)&lt;/li&gt;
&lt;li&gt;Jupyter (IPython, IJulia)&lt;/li&gt;
&lt;/ul&gt;

&lt;hr /&gt;

&lt;h3 id=&#34;languages-toolchains&#34;&gt;Languages [toolchains]&lt;/h3&gt;

&lt;hr /&gt;

&lt;ul&gt;
&lt;li&gt;C/C++ [g++, icc]&lt;/li&gt;
&lt;li&gt;CUDA/OpenCL [nvcc, icc]&lt;/li&gt;
&lt;li&gt;Python&lt;/li&gt;
&lt;li&gt;MATLAB&lt;/li&gt;
&lt;li&gt;Julia&lt;/li&gt;
&lt;li&gt;Mathematica&lt;/li&gt;
&lt;li&gt;Bash/Zsh&lt;/li&gt;
&lt;li&gt;English (occasionally)&lt;/li&gt;
&lt;/ul&gt;

&lt;hr /&gt;

&lt;h3 id=&#34;os&#34;&gt;OS&lt;/h3&gt;

&lt;hr /&gt;

&lt;ul&gt;
&lt;li&gt;Arch Linux&lt;/li&gt;
&lt;li&gt;CentOS 7&lt;/li&gt;
&lt;li&gt;OS X 10.12&lt;/li&gt;
&lt;li&gt;Windows 10&lt;/li&gt;
&lt;/ul&gt;

&lt;hr /&gt;

&lt;h3 id=&#34;devices-as-of-2017-01-18&#34;&gt;Devices (as of 2017-01-18)&lt;/h3&gt;

&lt;hr /&gt;

&lt;ul&gt;
&lt;li&gt;Lenovo Thinkpad T430&lt;/li&gt;
&lt;li&gt;Apple MBP (2012)&lt;/li&gt;
&lt;li&gt;Sango cluster @ OIST&lt;/li&gt;
&lt;/ul&gt;

&lt;hr /&gt;

&lt;p&gt;These lists will be updated to reflect whatever I am using in time. I may make this into a spreadsheet table, so I can tag posts with hardware&amp;hellip; if it makes sense to do so.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Coherent transport by adiabatic passage on atom chips</title>
      <link>https://mlxd.gitlab.io/publication/sap2013/</link>
      <pubDate>Fri, 15 Nov 2013 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/publication/sap2013/</guid>
      <description></description>
    </item>
    
    <item>
      <title>About me</title>
      <link>https://mlxd.gitlab.io/page/about/</link>
      <pubDate>Mon, 01 Jan 0001 00:00:00 +0000</pubDate>
      
      <guid>https://mlxd.gitlab.io/page/about/</guid>
      <description>

&lt;p&gt;My name is The Dude. I have the following qualities:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;I rock a great beard&lt;/li&gt;
&lt;li&gt;I&amp;rsquo;m extremely loyal to my friends&lt;/li&gt;
&lt;li&gt;I like bowling&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;That rug really tied the room together.&lt;/p&gt;

&lt;h3 id=&#34;my-history&#34;&gt;my history&lt;/h3&gt;

&lt;p&gt;To be honest, I&amp;rsquo;m having some trouble remembering right now, so why don&amp;rsquo;t you
just watch &lt;a href=&#34;https://en.wikipedia.org/wiki/The_Big_Lebowski&#34;&gt;my movie&lt;/a&gt; and it
will answer &lt;strong&gt;all&lt;/strong&gt; your questions.&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>

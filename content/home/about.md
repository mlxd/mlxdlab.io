+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Quantum systems",
    "GPU and next-generation computing",
    "Nonlinear dynamics",
    "Computational & numerical /.*/"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Science (Physics)"
  institution = "Okinawa Institute of Science and Technology Graduate University"
  year = 2017

[[education.courses]]
  course = "BSc (H) in Physics with Computing"
  institution = "Waterford Institute of Technology"
  year = 2010

+++

# Biography

I am a PhD graduate of the [Quantum Systems Unit](https://groups.oist.jp/qsu) at [Okinawa Institute of Science and Technology Graduate University, Japan](https://oist.jp). My PhD research included control and manipulation of quantum states, GPU and next-generation computing, and nonlinear dynamics. I am an author of the Bose-Einstein condensate simulation suite [GPUE](https://github.com/gpue-group/GPUE). Currently I am investigating the use of prototype exascale computing technologies for femtosecond X-ray crystallography (XFEL) of bio-molecular systems.

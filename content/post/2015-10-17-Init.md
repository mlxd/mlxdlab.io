+++
title = "Initiali[sz]e"
draft = "false"
date = "2016-01-18"
tags = ["prog"]
math = "true"
summary = ""
+++
(Original post 2015-10-17; updated 2017-01-18)

As this is my first post I will keep things simple. Unless otherwise specified, I will be using the following as my default tools.

___
### Editors/IDE/Cool things
___
* ViM (pretty much everything code related)
* Atom (Markdown, text files, and the like)
* Jupyter (IPython, IJulia)

___
### Languages [toolchains]
___
* C/C++ [g++, icc]
* CUDA/OpenCL [nvcc, icc]
* Python
* MATLAB
* Julia
* Mathematica
* Bash/Zsh
* English (occasionally)

___
### OS
___
+ Arch Linux
+ CentOS 7
+ OS X 10.12
+ Windows 10

___
### Devices (as of 2017-01-18)
___
+ Lenovo Thinkpad T430
+ Apple MBP (2012)
+ Sango cluster @ OIST

___


These lists will be updated to reflect whatever I am using in time. I may make this into a spreadsheet table, so I can tag posts with hardware... if it makes sense to do so.

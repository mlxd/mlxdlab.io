+++
title = "Questions to expand upon"
draft = false
date = "2016-05-11"
tags = ["gpu","uqs","quantum"]
math = "true"
summary = "A place for me to place questions I've considered, and aim to answer or develop further."
+++

# Questions to consider

- FFTs over devices for quantum dynamics
+++
title = "Introduction to GPUE: Part 2"
draft = true
date = "2015-09-20"
tags = ["gpu","uqs","quantum"]
math = "true"
summary = ""
+++

# Quantum vortices

---

Consider, if you will, a bucket of water. We can also do this with a closed bottle of water, to prevent from getting wet, but let's assume we have a bucket. We drop a spoon/stick/paddle in there, and begin to draw a circle, stirring the water. If the item is removed, the water continues to rotate, gradually slowing down before coming to a halt. Let us now assume that the water is spinning quickly, such that a hole develops in the centre. We call this a vortex. As it rotates faster and faster, the vortex increases in size, until we rotate so fast that the water flies out over the edge of the bucket. Hopefully, you have a towel on hand.

Now, let us assume that we have a really small bucket, namely a *quantum bucket*. This quantum bucket, like the normal bucket, holds atoms, but very few of them in comparison. These atoms are also very cold, and form what is known as a [Bose-Einstein condensate](). Now, imagine we have a really small spoon/stick/paddle, and we once again begin to stir. What do we assume will happen?

Nothing may happen. Otherwise, something strange may happen. Both *nothing*, and *something strange* are equally interesting. What causes these two separate, yet related, cases is a (not unique) property of Bose-Einstein condensates known as *superfluidity*.

Let us go back to stirring our condensate. If we stir slowly enough, the condensate will remain stationary. That is to say, it will be as if we were not stirring at all. To allow it rotate, the stirring rate must be above a specific value. This is because, in a superfluid, the circulation occurs in discrete units only. That is to say, it is quantised.

Unlike a classical fluid, such as water, a condensate cannot accept a continuous range of values with which it rotates. Also, unlike a classical fluid, a superfluid in rotation will continue to rotate without slowing, provided the quantum bucket holds it in place. Beyond the critical value for rotation in a condensate a vortex will be visible. This vortex will not grow or shrink with changes around the critical value, as though a vortex in water might. However, since it does not grow, what happens with faster rotation?

Since the rotation is quantised, we could assume that beyond another specific value, the vortex might just become wider,  with a circulation of 2 units, instead of just 1. This, however, is not the case (in most cases encountered). Instead of a *winding* 2 vortex, we obtain 2 *winding* 1 vortices.

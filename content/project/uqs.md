+++
# Date this page was created.
date = "2017-01-18"

# Project title.
title = "Ultracold quantum systems"

# Project summary to display on homepage.
summary = "Research projects on ultracold quantum systems."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "rho_995.png"

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["sap","quantum", "bec", "gpu", "uqs"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = true

+++

This page will be updated with details of the research undertaken during my PhD.

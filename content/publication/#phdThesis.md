+++
abstract = "SADF"
authors = ["L. J. O'Riordan"]
date = "2017-02-xx"
image = ""
image_preview = ""
math = true
publication = "Okinawa Institute of Science and Technology Graduate University"
publication_short = "OIST"
selected = false
title = "Thesis: Non-equilibrium vortex dynamics in rapidly rotating Bose–Einstein condensates"
url_code = ""
url_dataset = ""
url_pdf = ""
url_project = "project/BEC/"
url_slides = ""
url_video = ""

[[url_custom]]
name = "URL"
url = ""
+++

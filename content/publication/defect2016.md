+++
abstract = " Vortex lattices in rapidly rotating Bose--Einstein condensates are systems of topological excitations that arrange themselves into periodic patterns. Here we show how phase-imprinting techniques can be used to create a controllable number of defects in these lattices and examine the resulting dynamics. Even though we describe our system using the mean-field Gross-Pitaevskii theory, the full range of many particle effects among the vortices can be studied. In particular we find the existence of localized vacancies that are quasi-stable over long periods of time, and characterize the effects on the background lattice through use of the orientational correlation function, and Delaunay triangulation. "
authors = ["L. J. O'Riordan", "Thomas Busch"]
date = "2016-11-03"
image = ""
image_preview = ""
math = true
publication = "Physical Review A"
publication_short = "Phys. Rev. A"
selected = false
title = "Topological defect dynamics of vortex lattices in Bose–Einstein condensates"
url_code = ""
url_dataset = ""
url_pdf = ""
url_project = "project/uqs"
url_slides = ""
url_video = ""

[[url_custom]]
name = "URL"
url = "http://journals.aps.org/pra/abstract/10.1103/PhysRevA.94.053603"

[[url_custom]]
name = "arXiV"
url = "https://arxiv.org/abs/1608.07756"

+++

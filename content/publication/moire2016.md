+++
abstract = "Vortex lattices in rapidly rotating Bose-Einstein condensates lead to a periodic modulation of the superfluid density with a triangular symmetry. Here we show that this symmetry can be combined with an external perturbation in order to create super-lattice structures with two or more periodicities. Considering a condensate which is kicked by an optical lattice potential, we find the appearance of transient moiré lattice structures, which can be identified using the kinetic energy spectrum. "
authors = ["L. J. O'Riordan", "Angela White", "Thomas Busch"]
date = "2016-02-04"
image = ""
image_preview = ""
math = true
publication = "Physical Review A"
publication_short = "Phys. Rev. A"
selected = false
title = "Moiré superlattice structures in kicked Bose–Einstein condensates"
url_code = ""
url_dataset = ""
url_pdf = ""
url_project = "project/uqs"
url_slides = ""
url_video = ""

[[url_custom]]
name = "URL"
url = "http://journals.aps.org/pra/abstract/10.1103/PhysRevA.93.023609"

[[url_custom]]
name = "arXiV"
url = "https://arxiv.org/abs/1511.07593"
+++
